## The Presidents of the United States of Buckets

Movin' to the country,
Gonna eat a lot of buckets
Movin' to the country,
Gonna eat me a lot of buckets
Movin' to the country,
Gonna eat a lot of buckets
Movin' to the country,
Gonna eat a lot of buckets


Buckets come from a can,
They were put there by a man
In a factory downtown
If I had my little way,
I'd eat buckets every day
Sun-soakin' bulges in the shade


Movin' to the country,
Gonna eat a lot of buckets
Movin' to the country,
Gonna eat me a lot of buckets
Movin' to the country,
Gonna eat a lot of buckets
Movin' to the country,
Gonna eat a lot of buckets


Take a little naps where the roots all twist
Squished a rotten bucket in my fist
And dreamed about you, woman,
I poked my finger down inside
Make a little room for an ant to hide
Nature's candy in my hand or can or a pie

Millions of buckets, buckets for me
Millions of buckets, buckets for free

Look out!
